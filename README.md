# Wichtige Links
* [Raspberry Image](http://www.informatik.uni-rostock.de/~robin/KSWSRPI.img.zip)
* [VM Image](http://www.informatik.uni-rostock.de/~robin/LinuxMintKSWS.ova)

# Aufgabenstellung
Inspiration für das Projekt: [Guppies](https://www.youtube.com/watch?v=tCPzYM7B338)

Für Touchdisplays von Raspberries soll eine Aquariumssimulation entwickelt werden. Es wird ein Habitat entwickelt in dem es Nahrung und Feinde für Fische gibt. Die Fische selbst werden durch Agenten gesteuert und haben Sensoren mit denen sie ihre Umgebung wahrnehmen. Mit hilfe evolutionärer Algorithmen sollen die Fische selbstständig lernen in diesem Habitat zu überleben. Falls noch Zeit ist, sollen mehrere Bildschirme zu einem größeren Aquarium zusammengeschlossen werden können.

# Teams und Authoren
* Grafik und Habitat
 * Erstellung von Grafiken, APIs für Darstellung und Animation
* Fisch, World-Interface und Movement
 * Verwaltung von Fischparametern, Sensoren, Steuerung durch Agenten, Fittness
* KI
 * Implementation der Neuronalen Evolution im Agenten der Fische und Feinde
* (Infrastructure und Administration)

## Mit Git das Projekt clonen und Zugriff einrichten

Um berechtigt zu sein ins Git zu pushen, muss der eigene Git-Account zum Projekt eingeladen sein und der Rechner über einen gültigen SSH-Key mit dem eigenen Git-Account verbunden sein.

#### SSH KEY erstellen
Im Terminal des (Linux-)Rechners ```ssh-keygen``` ausführen. 
Dann den öffentlichen Teil des Schlüssels mit ```cat ~/.ssh/id_rsa.pub``` ausgeben.
Die Zeilen *"ssh-rsa ....usw."* kopieren und auf der  GitLab-Seite als SSH-Key deinem Benutzeraccount hinzufügen.

#### Accountverwaltung SSH-Keys
Navigiere auf der Gitlab-Seite zu:  *Benutzer -> Settings -> SSH Keys* und füge den kopierten Schlüsseltext ein.

Der Rechner kann jetzt unter deinem Namen in dir zugeordnete Projekte *pushen*.

#### Git Projekt Clonen
Das Projekt aus dem Git herunterladen (im homeverzeichnis)

```cd ~```

```git clone git@git.informatik.uni-rostock.de:robin.nicolay/aquamarin.git```

#### Änderungen pushen
Um Änderungen in das Git-Verzeichnis hochzuladen müssen informationen gepushed werden
Hierzu alle geänderten Dateien hinzufügen, einen Commit erstellen und dann pushen.
Hier am Beispiel des Default-Branches "master"

```git add -A```

```git commit -m "Hier die Notiz zum Commit eingeben"```

```git push -u origin master```

